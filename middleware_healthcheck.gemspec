$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "middleware_healthcheck/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "middleware_healthcheck"
  s.version     = MiddlewareHealthcheck::VERSION
  s.authors     = ["Adam Wieczorkowski", "Jan Wieczorkowski"]
  s.email       = ["adam.wieczorkowski@naturaily.com", "jan.wieczorkowski@naturaily.com"]
  s.homepage    = ""
  s.summary     = "Summary of MiddlewareHealthcheck."
  s.description = "Description of MiddlewareHealthcheck."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 4.0"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "pry-byebug"
end
