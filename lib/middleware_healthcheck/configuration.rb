module MiddlewareHealthcheck
  class Configuration
    DEFAULT_HEALTHCHECK_PATH = '/healthcheck'.freeze
    DEFAULT_FULL_CHECK_PARAM_NAME = 'full'.freeze
    DEFAULT_SELECTED_CHECK_PARAM_NAME = 'checks'.freeze
    DEFAULT_ERROR_RESPONSE_STATUS = 422
    DEFAULT_SUCCESS_RESPONSE_STATUS = 200
    DEFAULT_SUCCESS_RESPONSE_BODY = "It's alive!".freeze
    DEFAULT_ERRORS_DELIMITER = '; '.freeze
    DEFAULT_SELECTED_CHECK_PARAM_SPLIT_DELIMITER = ','.freeze

    attr_accessor :healthcheck_path, :full_check_param_name, :selected_check_param_name,
      :error_response_status, :success_response_status, :success_response_body,
      :errors_delimiter, :selected_check_param_split_delimiter, :checkers

    def initialize
      self.healthcheck_path = DEFAULT_HEALTHCHECK_PATH
      self.full_check_param_name = DEFAULT_FULL_CHECK_PARAM_NAME
      self.selected_check_param_name = DEFAULT_SELECTED_CHECK_PARAM_NAME
      self.error_response_status = DEFAULT_ERROR_RESPONSE_STATUS
      self.success_response_status = DEFAULT_SUCCESS_RESPONSE_STATUS
      self.success_response_body = DEFAULT_SUCCESS_RESPONSE_BODY
      self.errors_delimiter = DEFAULT_ERRORS_DELIMITER
      self.selected_check_param_split_delimiter = DEFAULT_SELECTED_CHECK_PARAM_SPLIT_DELIMITER
      self.checkers = MiddlewareHealthcheck::DefaultCheckers.constants.map do |const|
        klass = MiddlewareHealthcheck::DefaultCheckers.const_get(const)
        klass if klass.is_a? Class
      end.compact
    end
  end
end
